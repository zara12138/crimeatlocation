package com.example.crimeatlocation.network.utils

import com.google.gson.JsonObject
import java.util.*

object Variables {

    // initial value is London
    var currentCamLat: Double = 51.50853
    var currentCamLng: Double = -0.12574

    var selectedDate: String = ""
    var crimeList: List<JsonObject> = arrayListOf()
}