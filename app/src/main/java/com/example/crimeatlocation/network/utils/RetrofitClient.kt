package com.example.crimeatlocation.network.utils

import com.example.crimeatlocation.network.request.CrimeRequest
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

class RetrofitClient {

    private val crimeRetrofit: Retrofit

    private val client: OkHttpClient = OkHttpClient.Builder()
        .connectTimeout(100, TimeUnit.SECONDS)
        .readTimeout(100, TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .build()

    private val gson: Gson = GsonBuilder()
        .registerTypeAdapter(Date::class.java, JsonDeserializer<Date> { json, _, _ ->
            if (json.asJsonPrimitive.isNumber)
                Date(json.asJsonPrimitive.asLong) else
                null
        })
        .serializeNulls()
        .create()

    val crimeRequest: CrimeRequest

    init {
        crimeRetrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()

        crimeRequest = crimeRetrofit.create(CrimeRequest::class.java)
    }

    companion object {
        val instance by lazy {
            RetrofitClient()
        }
        private const val BASE_URL = "https://data.police.uk/api/"
    }
}