package com.example.crimeatlocation.network.utils

import android.content.ContentValues.TAG
import android.util.Log
import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class CommonUtils {

    fun getTimeInFormat(date: Date): String {
        val cal = Calendar.getInstance()
        cal.time = date
        return "${cal.get(Calendar.YEAR)}-${cal.get(Calendar.MONTH)}"
    }

    fun getCrimeInformation(date: String, lat: Double, lng: Double, callback: MyCallback) {
        RetrofitClient.instance.crimeRequest
            .getCrimesAtLocationWithDate(date, lat, lng)
            .enqueue(object : Callback<List<JsonObject>> {
                override fun onFailure(call: Call<List<JsonObject>>, t: Throwable) {
                    callback.onFailure()
                    Log.e(TAG, "failed to get crime information", t)
                }

                override fun onResponse(
                    call: Call<List<JsonObject>>,
                    response: Response<List<JsonObject>>
                ) {
                    if (response.isSuccessful) {
                        Variables.crimeList = response.body()!!
                        callback.onResponse(response.body()!!)
                    }
                }

            })
    }

    interface MyCallback {
        fun onFailure()
        fun onResponse(result: List<JsonObject>)
    }
}