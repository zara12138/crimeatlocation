package com.example.crimeatlocation.network.request

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface CrimeRequest {

    @GET("crimes-at-location")
    fun getCrimesAtLocationWithDate(
        @Query("date") date: String,
        @Query("lat") lat: Double,
        @Query("lng") lng: Double): Call<List<JsonObject>>
}