package com.example.crimeatlocation.view

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.eftimoff.androipathview.PathView
import com.example.crimeatlocation.R
import com.example.crimeatlocation.network.utils.CommonUtils
import com.example.crimeatlocation.network.utils.Variables
import com.google.gson.JsonObject
import java.util.*

class SplashFragment : Fragment(), PathView.AnimatorBuilder.ListenerEnd {

    private lateinit var animatedSplashLogo: PathView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val splashScreenRootView = inflater.inflate(R.layout.fragment_splash, container, false)

        animatedSplashLogo = splashScreenRootView.findViewById(R.id.splash_screen_view)
        animatedSplashLogo.useNaturalColors()
        animatedSplashLogo.setFillAfter(true)
        animatedSplashLogo.pathAnimator
            .interpolator(AccelerateDecelerateInterpolator())
            .listenerEnd(this)
            .delay(200)
            .duration(1500)
            .start()

        return splashScreenRootView
    }

    override fun onAnimationEnd() {
        CommonUtils().getCrimeInformation(
            CommonUtils().getTimeInFormat(Date()),
            Variables.currentCamLat,
            Variables.currentCamLng,
            object : CommonUtils.MyCallback {
                override fun onFailure() {
                    Toast.makeText(context, "Something wrong with the server", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(result: List<JsonObject>) {
                    Log.i(TAG, "response: $result")

                    findNavController().navigate(
                        R.id.action_splashFragment_to_mapFragment,
                        null,
                        NavOptions.Builder().setPopUpTo(R.id.splashFragment, true).build()
                    )
                }

            })
    }

}
