package com.example.crimeatlocation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.crimeatlocation.R
import com.example.crimeatlocation.network.utils.CommonUtils
import com.example.crimeatlocation.network.utils.Variables
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.JsonObject
import java.util.*

class MapFragment : Fragment(), OnMapReadyCallback,
    GoogleMap.OnCameraIdleListener, View.OnClickListener {

    private var mMap: GoogleMap? = null
    private lateinit var dateInput: EditText
    private lateinit var dateInputLayout: TextInputLayout
    private lateinit var searchBtn: ImageView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_map, container, false)

        val fm = childFragmentManager
        var mapFragment = fm.findFragmentById(R.id.map) as SupportMapFragment?

        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance()
            fm.beginTransaction().replace(R.id.map, mapFragment).commit()
        }

        mapFragment!!.getMapAsync(this)

        dateInput = rootView.findViewById(R.id.time)
        dateInput.hint = resources.getString(R.string.date_input)
        if (Variables.selectedDate.isNotBlank())
            dateInput.setText(Variables.selectedDate)

        dateInputLayout = rootView.findViewById(R.id.textInputLayout)

        searchBtn = rootView.findViewById(R.id.search)
        searchBtn.setOnClickListener(this)
        return rootView
    }

    override fun onMapReady(map: GoogleMap?) {
        val currentLocation = LatLng(Variables.currentCamLat, Variables.currentCamLng)

        mMap = map
        mMap?.setOnCameraIdleListener(this)
        mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 10f))

        mMap?.setOnMarkerClickListener {
            findNavController().navigate(R.id.action_mapFragment_to_crimeListFragment)
            true
        }
    }

    override fun onCameraIdle() {

        mMap?.cameraPosition?.target?.let {
            Variables.currentCamLat = it.latitude
            Variables.currentCamLng = it.longitude
        }

        val date = if (Variables.selectedDate != "")
            Variables.selectedDate
        else CommonUtils().getTimeInFormat(Date())

        getCrimeInfoToDisplay(
            date,
            Variables.currentCamLat,
            Variables.currentCamLng
        )
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.search -> {
                if (dateInput.text.isBlank()) {
                    dateInputLayout.error = "couldn't be empty"
                } else {
                    dateInputLayout.isErrorEnabled = false
                    Variables.selectedDate = dateInput.text.toString()
                    getCrimeInfoToDisplay(
                        dateInput.text.toString(),
                        Variables.currentCamLat,
                        Variables.currentCamLng
                    )
                }
            }
        }
    }

    private fun getCrimeInfoToDisplay(date: String, lat: Double, lng: Double) {
        CommonUtils().getCrimeInformation(date, lat, lng, object : CommonUtils.MyCallback {
            override fun onFailure() {
                Toast.makeText(context, "Oops, something wrong with the server.", Toast.LENGTH_LONG).show()
            }

            override fun onResponse(result: List<JsonObject>) {
                displayMarkers()
            }
        })
    }

    private fun displayMarkers() {
        mMap?.clear()
        for (crimeJsonObj in Variables.crimeList) {
            val latitude = crimeJsonObj["location"].asJsonObject["latitude"].asDouble
            val longitude = crimeJsonObj["location"].asJsonObject["longitude"].asDouble
            val latLng = LatLng(latitude, longitude)
            mMap?.addMarker(MarkerOptions().position(latLng))
        }
    }

}
