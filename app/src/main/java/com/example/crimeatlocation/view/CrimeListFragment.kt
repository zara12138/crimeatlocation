package com.example.crimeatlocation.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.crimeatlocation.R
import com.example.crimeatlocation.network.utils.Variables
import com.example.crimeatlocation.view.adapters.CrimeAdapter

class CrimeListFragment : Fragment() {

    private lateinit var crimeListRecyclerView: RecyclerView
    private val crimeAdapter = CrimeAdapter(Variables.crimeList)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_crime_list, container, false)

        crimeListRecyclerView = rootView.findViewById(R.id.crime_list)
        crimeListRecyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        crimeListRecyclerView.adapter = crimeAdapter

        return rootView
    }

}
