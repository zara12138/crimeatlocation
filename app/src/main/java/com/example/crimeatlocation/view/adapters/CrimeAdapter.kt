package com.example.crimeatlocation.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.crimeatlocation.R
import com.google.gson.JsonObject
import kotlinx.android.synthetic.main.crime_list_item.view.*

class CrimeAdapter(private val crimeList: List<JsonObject>) :
    RecyclerView.Adapter<CrimeAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(crimeItem: JsonObject) {
            itemView.crime_id.text = String.format(
                itemView.resources.getString(R.string.crime_id),
                crimeItem["id"].asString
            )

            itemView.category.text = String.format(
                itemView.resources.getString(R.string.category),
                crimeItem["category"].asString
            )
            itemView.location_type.text = String.format(
                itemView.resources.getString(R.string.location_type),
                crimeItem["location_type"].asString
            )
            itemView.street_name.text = String.format(
                itemView.resources.getString(R.string.street),
                crimeItem["location"].asJsonObject["street"].asJsonObject["name"].asString
            )
            itemView.month.text = String.format(
                itemView.resources.getString(R.string.month),
                crimeItem["month"].asString
            )

            if (crimeItem["outcome_status"].isJsonNull)
                itemView.outcome_status_category.text =
                    itemView.resources.getString(R.string.no_outcome_status)
            else
                itemView.outcome_status_category.text = String.format(
                    itemView.resources.getString(R.string.outcome_status),
                    crimeItem["outcome_status"].asJsonObject.get("category").asString
                )

            itemView.latitude.text = String.format(
                itemView.resources.getString(R.string.latitude),
                crimeItem["location"].asJsonObject["latitude"].asString
            )
            itemView.longitude.text = String.format(
                itemView.resources.getString(R.string.longitude),
                crimeItem["location"].asJsonObject["longitude"].asString
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.crime_list_item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return crimeList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(crimeList[position])
    }

}